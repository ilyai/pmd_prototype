$(function() {
  console.log("ready");

  var margin = 50;
  var width = 700;
  var height = 300;

  var data = [];
  _.times(100, function(i) {
    data.push({
      temp: _.random(-15.3, 34.8),
      day: _.now() + (i * 1000000000)
    });
  });

  d3.select(".graph")
      .append("svg")
        .attr("width", width)
        .attr("height", height)
      .selectAll("circle")
      .data(data)
      .enter()
      .append("circle");

  var extentX = d3.extent(data, function(d) { return d.day; });
  var extentY = d3.extent(data, function(d) { return d.temp; });
  console.log(new Date(extentX[0]));
  console.log(new Date(extentX[1]));

  var scaleX = d3.scale.linear()
    .range([margin, width-margin])
    .domain(extentX);

  var scaleY = d3.scale.linear()
    .range([height-margin, margin])
    .domain(extentY);

  d3.selectAll("circle")
    .attr("r", 5)
    .attr("cx", function(d) { return scaleX(d.day); })
    .attr("cy", function(d) { return scaleY(d.temp); });

  var axisX = d3.svg.axis().scale(scaleX);
  var axisY = d3.svg.axis().scale(scaleY).orient("left");
  
  d3.select("svg")
    .append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + (height-margin) + ")")
    .call(axisX);

  d3.select("svg")
    .append("g")
      .attr("class", "y axis")
      .attr("transform", "translate(" + margin + ", 0)")
    .call(axisY);

  d3.select(".x.axis")
    .append("text")
      .text("Day")
      .attr("x", (width/2)-margin)
      .attr("y", margin / 1.5);

  d3.select(".y.axis")
    .append("text")
      .text("Temperature, C")
      .attr("transform", "rotate(-90, -30, 0) translate(-220)");
});
